Feature: Project
  Scenario: I want to create a project
    Given I had authentication to todo.ly
    When I send POST request 'projects.json' with json and BASIC authentication
    """
    {
      "Content": "API Automation test for items 2",
      "ItemsCount": "0",
      "Icon": "5",
      "ItemType": "2"
    }
    """
    Then I will expect the response code 200
    And I expected the response body is equal
    """
    {
      "Id": EXCLUDE,
      "Content": "API Automation test for items 2",
      "ItemsCount": 0,
      "Icon": 5,
      "ItemType": 2,
      "ParentId": null,
      "Collapsed": false,
      "ItemOrder": EXCLUDE,
      "Children": [],
      "IsProjectShared": false,
      "ProjectShareOwnerName": null,
      "ProjectShareOwnerEmail": null,
      "IsShareApproved": false,
      "IsOwnProject": true,
      "LastSyncedDateTime": EXCLUDE,
      "LastUpdatedDate": EXCLUDE,
      "Deleted": false,
      "SyncClientCreationId": null
    }
    """

  Scenario: I want to get all projects
    Given I had authentication to todo.ly
    When I send GET request 'projects.json' with json and BASIC authentication
    """
    {
      "Content": EXCLUDE
    }
    """
    Then I will expect the response code 200

  Scenario: I want to get a project by it's id
    Given I had authentication to todo.ly
    When I send GET request 'projects/4026386.json' with json and BASIC authentication
    """
    {
      "Content": EXCLUDE
    }
    """
    Then I will expect the response code 200


  Scenario: I want to update a project by its' id
    Given I had authentication to todo.ly
    When I send POST request 'projects/4026477.json' with json and BASIC authentication
    """
    {
      "Content": "API test PUT updated",
      "Icon": 4
    }
    """
    Then I will expect the response code 200

  Scenario: I want to delete a project by its' id
    Given I had authentication to todo.ly
    When I send DELETE request 'projects/4026477.json' with json and BASIC authentication
    """
    {
      "Content": EXCLUDE
    }
    """
    Then I will expect the response code 200
    And I expected the response body is equal
    """
    {
        "Id": 4026443,
        "Content": "API test PUT updated",
        "ItemsCount": 0,
        "Icon": 4,
        "ItemType": 2,
        "ParentId": null,
        "Collapsed": false,
        "ItemOrder": EXCLUDE,
        "Children": EXCLUDE
    }
    """