package runner;

import com.google.gson.*;

import clientAPI.FactoryRequest;
import clientAPI.RequestInformation;
import clientAPI.ResponseInformation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.JsonHelper;
import org.json.JSONException;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

import static configuration.Configuration.*;
import static junit.framework.TestCase.assertTrue;

public class MyStepdefs {

    ResponseInformation response = new ResponseInformation();
    Map<String, String> variables = new HashMap<>();



    @Given("^I had authentication to todo\\.ly$")
    public void iHaveAuthenticationToTodoLy() {

    }

    @When("^I send (POST|PUT|DELETE|GET) request '(.*)' with json and (BASIC|TOKEN) authentication$")
    public void sendRequest(String method, String url, String authentication, String jsonBody) {
        RequestInformation request = new RequestInformation();
        request.setUrl(HOST + this.replaceVariables(url));
        request.setBody(this.replaceVariables(jsonBody));
        if (authentication.equals("TOKEN")) {
            request.addHeaders(TOKEN_AUTHENTICATION_HEADER, authentication);
        } else {
            request.addHeaders(BASIC_AUTHENTICATION_HEADER, "Basic Y2FtaWxvY0BnbWFpbC5jb206NzMwMTE=");
        }

        response = FactoryRequest.make(method.toLowerCase()).send(request);

    }


    @And("^I expected the response body is equal$")
    public void iExpectedTheResponseBodyIsEqual(String expectResponseBody) throws JSONException {
        System.out.println("Response Body" + this.replaceVariables(response.getResponseBody()));
        Assert.assertTrue("ERROR el response body es incorrecto", JsonHelper.areEqualJSON(this.replaceVariables(expectResponseBody), response.getResponseBody()));
    }

    @Then("^I will expect the response code (\\d+)$")
    public void iExpectedTheResponseCode(int expectResponseCode) {
        System.out.println("Response code " + response.getResponseCode());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(JsonParser.parseString(this.replaceVariables(response.getResponseBody()))));
        // System.out.println("Response Body" + this.replaceVariables(response.getResponseBody()));
        Assert.assertEquals("ERROR !! El response body es incorrecto", expectResponseCode, response.getResponseCode());
    }

    private String replaceVariables(String value) {
        for (String key : this.variables.keySet()
        ) {
            value = value.replace(key, this.variables.get(key));
        }
        return value;
    }

    @And("^I get the property value '(.*)' and save on (.*)$")
    public void iGetThePropertyValueIdAndSaveOnID_USER(String property, String nameVariable) throws JSONException {
        String value = JsonHelper.getValueFromJSON(response.getResponseBody(), property);
        variables.put(nameVariable, value);
        System.out.println("variable : " + nameVariable + " value : " + variables.get(nameVariable));

    }
}
