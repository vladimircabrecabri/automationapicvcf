package configuration;

public class Configuration {

    public static final String HOST = "https://todo.ly/api/";
    public static String BASIC_AUTHENTICATION = "";
    public static final String BASIC_AUTHENTICATION_HEADER = "Authorization";
    public static final String TOKEN_AUTHENTICATION_HEADER = "Token";
}
