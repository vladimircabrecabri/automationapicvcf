Feature: Item

  Scenario: I want to create an item
    Given I had authentication to todo.ly
    When I send POST request 'items.json' with json and BASIC authentication
    """
    {
      "Content": "Test Item to update",
      "ProjectId": "4026450",
      "Priority": 2
    }
    """
    Then I will expect the response code 200
    And I expected the response body is equal
    """
    {
    "Id": EXCLUDE,
    "Content": "Test Item to update",
    "ItemType": 1,
    "Checked": false,
    "ProjectId": 4026450,
    "ParentId": null,
    "Path": "",
    "Collapsed": false,
    "DateString": null,
    "DateStringPriority": 0,
    "DueDate": "",
    "Recurrence": null,
    "ItemOrder": EXCLUDE,
    "Priority": 2,
    "LastSyncedDateTime": EXCLUDE,
    "Children": [],
    "DueDateTime": null,
    "CreatedDate": EXCLUDE,
    "LastCheckedDate": null,
    "LastUpdatedDate": EXCLUDE,
    "Deleted": false,
    "Notes": "",
    "InHistory": false,
    "SyncClientCreationId": null,
    "DueTimeSpecified": true,
    "OwnerId": 705998
    }
    """


  Scenario: I want to get all items
    Given I had authentication to todo.ly
    When I send GET request 'items.json' with json and BASIC authentication
    """
    {
      "Content": EXCLUDE
    }
    """
    Then I will expect the response code 200

  Scenario: I want to get an item by its' id
    Given I had authentication to todo.ly
    When I send GET request 'items/11067831.json' with json and BASIC authentication
    """
    {
      "Content": EXCLUDE
    }
    """
    Then I will expect the response code 200

  Scenario: I want to update an item by its' id
    Given I had authentication to todo.ly
    When I send POST request 'items/11067831.json' with json and BASIC authentication
    """
    {
      "Content": "Item updated"
    }
    """
    Then I will expect the response code 200

  Scenario: I want to delete an item by its' id
    Given I had authentication to todo.ly
    When I send DELETE request 'items/11067831.json' with json and BASIC authentication
    """
    {
      "Content": EXCLUDE
    }
    """
    Then I will expect the response code 200
