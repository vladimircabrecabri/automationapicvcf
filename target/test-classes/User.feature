Feature: User

  Scenario: I want to create a new user
    Given I had authentication to todo.ly
    When I send POST request 'user.json' with json and BASIC authentication
    """
    {
      "Email": "camiloc@gmail.com",
      "FullName": "Camilo Cabrera F",
      "Password": "73011"
    }
    """

    Then I will expect the response code 200

    And I expected the response body is equal
    """
    {
        "Id": EXCLUDE,
        "Email": "alefuo@gmail.com",
        "Password": null,
        "FullName": "Pedro Cabrera",
        "TimeZone": 0,
        "IsProUser": false,
        "DefaultProjectId": EXCLUDE,
        "AddItemMoreExpanded": false,
        "EditDueDateMoreExpanded": false,
        "ListSortType": 0,
        "FirstDayOfWeek": 0,
        "NewTaskDueDate": EXCLUDE,
        "TimeZoneId": EXCLUDE
    }
    """
    And I get the property value 'Id' and save on ID_USER

    Given I had authentication to todo.ly
    When I send GET request 'user.json' with json and BASIC authentication
    """
    {
       "Email": EXCLUDE,
       "FullName": EXCLUDE,
       "Password": EXCLUDE
    }
    """

    Then I will expect the response code 200

    Given I had authentication to todo.ly
    When I send PUT request 'user/ID_USER.json' with json and BASIC authentication
    """
    {
      "FullName": "CamiloK Cabrera"
    }
    """

    Then I will expect the response code 200
    And I expected the response body is equal
    """
    {
        "Id": EXCLUDE,
        "Email": "alefuo@gmail.com",
        "Password": null,
        "FullName": "CamiloK Cabrera",
        "TimeZone": 0,
        "IsProUser": false,
        "DefaultProjectId": EXCLUDE,
        "AddItemMoreExpanded": false,
        "EditDueDateMoreExpanded": false,
        "ListSortType": 0,
        "FirstDayOfWeek": 0,
        "NewTaskDueDate": EXCLUDE,
        "TimeZoneId": EXCLUDE
    }
    """

    Given I had authentication to todo.ly
    When I send DELETE request 'user/ID_USER.json' with json and BASIC authentication
    """
    {
         "FullName": EXCLUDE
    }
    """
    Then I will expect the response code 200